<?php
namespace Drupal\common_overrides\Controller;

use Drupal\search\SearchPageInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\search\Controller\SearchController;

/**
 * Override the Route controller for search.
 */
class CommonOverridesSearchController extends SearchController {

  /**
   * {@inheritdoc}
   */
  public function view(Request $request, SearchPageInterface $entity) {
    $build = parent::view($request, $entity);

    $config = \Drupal::config('common_overrides.settings');

    // Set the Result title.
    if (isset($build['search_results_title'])) {
      $tag = $config->get('search_results_tag');
      $title = $config->get('search_results_title');
      $build['search_results_title']['#markup'] = "<$tag>$title</$tag>";
    }

    return $build;
  }

}


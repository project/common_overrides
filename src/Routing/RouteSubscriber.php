<?php
/**
 * @file
 * Contains \Drupal\common_overrides\Routing\RouteSubscriber.
 */

namespace Drupal\common_overrides\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {
  
  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Replace dynamically created "search.view_node_search" route's Controller
    // with our own.
    if ($route = $collection->get('search.view_node_search')) {
      $route->setDefault('_controller', '\Drupal\common_overrides\Controller\CommonOverridesSearchController::view');
    }
  }
}

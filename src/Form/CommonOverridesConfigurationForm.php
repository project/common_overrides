<?php

namespace Drupal\common_overrides\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Defines a form that configures Common Overrides module settings.
 */
class CommonOverridesConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'common_overrides_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    // Set which config we're using.
    $config = $this->config('common_overrides.settings');

    // Show some help text.
    $form['help'] = [
      '#type'   => 'item',
      '#title'  => t('Instructions'),
      '#markup' => t($this->getHelpText()),
    ];

    //
    $form['search_results'] = [
      '#type'  => 'fieldset',
      '#title' => $this
        ->t('Search Results'),
    ];

    // Build the Mobile Breakpoint form element.
    $form['search_results']['search_results_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Search Results Text'),
      '#default_value' => $config->get('search_results_title'),
      '#description'   => $this->t(
        'The text to display above the search results.'
      ),
    ];

    $form['search_results']['search_results_tag'] = [
      '#type' => 'select',
      '#title' => $this->t('Search Results Tag'),
      '#default_value' => $config->get('search_results_tag'),
      '#options' => [
        'h1' => $this->t('h1'),
        'h2' => $this->t('h2'),
        'h3' => $this->t('h3'),
        'h4' => $this->t('h4'),
        'h5' => $this->t('h5'),
        'h6' => $this->t('h6'),
      ],
      '#description' => $this->t('The tag to wrap the search results title in.'),
    ];

    // Hand off the form build and return results.
    return parent::buildForm($form, $form_state);
  }

  /**
   * The help text rendered on the Common Overrides configuration page.
   *
   * @return string
   */
  public function getHelpText(): string {
    return "
    <p>Choose below which items you'd like to configure.</p>
    <p>Overrides will become available as they come up.</p>
    ";
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Set which config we're using.
    $config = $this->config('common_overrides.settings');


    // Set the Search Results Title config.
    $config->set('search_results_title', $form_state->getValue('search_results_title'))
      ->save();

    // Set the Save Search Results Tag config.
    $config->set('search_results_tag', $form_state->getValue('search_results_tag'))
      ->save();

    // Call the parent submitForm to handle the remainder of the submission.
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'common_overrides.settings',
    ];
  }

}
